# REST APIs

- [REST APIs](#rest-apis)
  - [Get Latest Currency Rates](#get-latest-currency-rates)
    - [Description](#description)
    - [Query Parameters](#query-parameters)
    - [Example Request](#example-request)
    - [Example Response](#example-response)

## Get Latest Currency Rates

| GET | /api/rates |
| --- | ---------- |

### Description

Gets latest exchange rates for all available currency.

### Query Parameters

| Field Name | Data Type | Required | Description                 | Example |
| ---------- | --------- | -------- | --------------------------- | ------- |
| base       | string    | False    | Base currency. Default: HKD | HKD     |

### Example Request

```shell
curl 'https://demo.andytang.dev/currency-converter/api/rates?base=HKD'
```

### Example Response

```xml
<?xml version="1.0" encoding="UTF-8"?>
<ExRate>
  <rates>
    <CHF>0.1161</CHF>
    <HRK>0.7936</HRK>
    <MXN>2.5721</MXN>
    <ZAR>1.769</ZAR>
    <INR>9.4213</INR>
    <CNY>0.8229</CNY>
    <THB>4.0145</THB>
    <AUD>0.1666</AUD>
    <ILS>0.4193</ILS>
    <KRW>143.4202</KRW>
    <JPY>14.142</JPY>
    <PLN>0.4722</PLN>
    <GBP>0.0911</GBP>
    <IDR>1839.3136</IDR>
    <HUF>36.5988</HUF>
    <PHP>6.167</PHP>
    <TRY>1.1067</TRY>
    <RUB>9.4668</RUB>
    <ISK>15.4513</ISK>
    <HKD>1.0</HKD>
    <EUR>0.1058</EUR>
    <DKK>0.7865</DKK>
    <USD>0.1289</USD>
    <CAD>0.1555</CAD>
    <MYR>0.5316</MYR>
    <BGN>0.2068</BGN>
    <NOK>1.0723</NOK>
    <RON>0.5204</RON>
    <SGD>0.1706</SGD>
    <CZK>2.6931</CZK>
    <SEK>1.0671</SEK>
    <NZD>0.1781</NZD>
    <BRL>0.6651</BRL>
  </rates>
  <base>HKD</base>
  <date>1622592000000</date>
</ExRate>
```

---
