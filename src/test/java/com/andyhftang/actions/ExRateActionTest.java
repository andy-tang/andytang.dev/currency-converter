package com.andyhftang.actions;

import com.opensymphony.xwork2.ActionSupport;

import org.apache.struts2.StrutsTestCase;

public class ExRateActionTest extends StrutsTestCase {

    public void testExRateAction() throws Exception {
        assertEquals(ActionSupport.SUCCESS, new RatesAction().execute());
    }
}
