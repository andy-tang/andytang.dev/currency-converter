package com.andyhftang.actions;

import com.opensymphony.xwork2.ActionSupport;

import org.apache.struts2.StrutsTestCase;

public class ConverterActionTest extends StrutsTestCase {

    public void testConverterAction() throws Exception {
        assertEquals(ActionSupport.SUCCESS, new ConverterAction().execute());
    }
}
