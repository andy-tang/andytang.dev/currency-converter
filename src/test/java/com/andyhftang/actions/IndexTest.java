package com.andyhftang.actions;

import com.opensymphony.xwork2.ActionSupport;

import org.apache.struts2.StrutsTestCase;

public class IndexTest extends StrutsTestCase {

    public void testIndex() throws Exception {
        assertEquals(ActionSupport.SUCCESS, new Index().execute());
    }
}
