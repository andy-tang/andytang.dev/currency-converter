package com.andyhftang;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ognl.OgnlRuntime;

/**
 * To allow works Struts 2 with Google App Engine
 */
public class InitListener implements ServletContextListener {
    private Logger log = LogManager.getLogger(LogManager.ROOT_LOGGER_NAME);

    public InitListener() {
        log.info("Application initializing");
    }

    public void contextInitialized(ServletContextEvent sce) {
        OgnlRuntime.setSecurityManager(null);
    }

    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Application shutting down");
    }

}
