package com.andyhftang.actions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opensymphony.xwork2.Preparable;

import lombok.Getter;

@Getter
public class ConverterAction extends GeneralAction implements Preparable {
    private static final long serialVersionUID = 1L;
    private static final ObjectMapper mapper = new ObjectMapper();
    private String baseUrl;
    private String[] currencies;

    @Override
    public void prepare() throws Exception {
        this.baseUrl = this.appHost + this.appContextPath;
        try {
            this.currencies = mapper.readValue(this.appCurrencies, String[].class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }
}
