
package com.andyhftang.actions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import com.andyhftang.services.ExRateService;
import com.andyhftang.services.ExRateService.ExRate;
import com.andyhftang.utils.xml.Xml;
import com.opensymphony.xwork2.ActionSupport;

import org.xml.sax.SAXException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RatesAction extends ActionSupport {
    private static final long serialVersionUID = 1L;
    private transient InputStream inputStream;
    private String base = "HKD";

    @Override
    public String execute() {
        ExRate rates = null;

        try {
            rates = ExRateService.fetchExRate();
        } catch (IOException | XPathExpressionException | SAXException | ParseException e) {
            e.printStackTrace();
            return String.valueOf(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        }

        rates.toBase(this.base);

        this.inputStream = new ByteArrayInputStream(Xml.ofObject(rates).getBytes());
        return SUCCESS;
    }

}
