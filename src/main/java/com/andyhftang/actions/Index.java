package com.andyhftang.actions;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.Preparable;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import lombok.Getter;

@Getter
@Results({ @Result(name = Action.SUCCESS, location = "${redirectName}", type = "redirectAction") })
public class Index extends GeneralAction implements Preparable {
    private static final long serialVersionUID = 1L;
    private static final String HOME_PAGE = "converter";
    private String redirectName;

    @Override
    public void prepare() throws Exception {
        this.redirectName = (this.appContextPath.isEmpty() ? "" : this.appContextPath.substring(1) + "/") + HOME_PAGE;
    }

    @Override
    public String execute() {
        return Action.SUCCESS;
    }
}
