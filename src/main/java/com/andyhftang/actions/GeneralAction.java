package com.andyhftang.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;

public abstract class GeneralAction extends ActionSupport {
    private static final long serialVersionUID = 1L;

    // Application configuration
    @Inject("app.host")
    protected String appHost;

    @Inject("app.contextPath")
    protected String appContextPath;

    @Inject("app.currencies")
    protected String appCurrencies;

}