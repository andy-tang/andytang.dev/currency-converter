package com.andyhftang.utils.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public final class Xml {
    private static final XmlMapper xmlMapper = new XmlMapper();

    private Xml() {
    }

    public static String ofObject(Object obj) {
        try {
            return xmlMapper.writeValueAsString(obj);
        } catch (Exception e) {
            return "";
        }
    }
}
