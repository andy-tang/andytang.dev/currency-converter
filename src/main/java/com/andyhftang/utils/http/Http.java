package com.andyhftang.utils.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public final class Http {
    private Http() {
    }

    public static String fetch(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (Exception e) {
            return null;
        }
        return stringBuilder.toString();
    }

}
