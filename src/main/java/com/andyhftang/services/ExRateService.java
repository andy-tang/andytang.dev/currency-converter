package com.andyhftang.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.andyhftang.utils.http.Http;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import lombok.AllArgsConstructor;
import lombok.Data;

public class ExRateService {
    private static final String API_HOST = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    private static final URL API_URL = createApiUrl(API_HOST);
    private static final DocumentBuilder documentBuilder = createDocumentBuilder();

    private static URL createApiUrl(String host) {
        try {
            return UriBuilder.fromUri(host).build().toURL();
        } catch (final MalformedURLException e) {
            throw new ArithmeticException("Cannot cerate URL instance");
        }
    }

    private static DocumentBuilder createDocumentBuilder() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setExpandEntityReferences(false);
        try {
            documentBuilderFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            documentBuilderFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            return documentBuilderFactory.newDocumentBuilder();
        } catch (final ParserConfigurationException e) {
            throw new ArithmeticException("Cannot cerate DocumentBuilder instance");
        }

    }

    private ExRateService() {
    }

    @Data
    @AllArgsConstructor
    public static class ExRate {
        private Map<String, Double> rates;
        private String base;
        private Date date;

        public void toBase(String base) {
            if (!rates.containsKey(base))
                throw new IllegalArgumentException("base currency does not exist in the rates table");

            this.base = base;
            Double baseRate = rates.get(base);
            rates.forEach((key, value) -> rates.put(key, Math.round((value / baseRate) * 10000) / 10000.0));
        }
    }

    public static ExRate fetchExRate() throws IOException, SAXException, XPathExpressionException, ParseException {
        // Fetch API
        String apiResp = Http.fetch(API_URL);
        if (apiResp == null)
            throw new IOException("Cannot fetch data from external API");

        // Parse XML contents
        Document document = documentBuilder.parse(new ByteArrayInputStream(apiResp.getBytes(StandardCharsets.UTF_8)));
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList cubeNode = (NodeList) xPath.compile("//*[name()='gesmes:Envelope']/Cube/Cube").evaluate(document,
                XPathConstants.NODESET);
        if (cubeNode.getLength() == 0)
            throw new ParseException("API response format is empty", 0);

        // Prepare result
        Element cubeElement = (Element) cubeNode.item(0);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(cubeElement.getAttribute("time"));

        Map<String, Double> rates = new HashMap<>();
        rates.put("EUR", 1.0);
        NodeList rateNodes = cubeElement.getChildNodes();
        for (int i = 0; i < rateNodes.getLength(); i++) {
            if (rateNodes.item(i).getNodeType() != Node.ELEMENT_NODE)
                continue;
            Element element = (Element) rateNodes.item(i);
            rates.put(element.getAttribute("currency"), Double.parseDouble(element.getAttribute("rate")));
        }

        return new ExRate(rates, "EUR", date);
    }

}