<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" tagdir="/WEB-INF/tags" %>

<z:layout>
  <section class="billboard">
    <div>
      <h3><s:text name="converter.billboard.title" /></h3>
      <p id="date">
        <span><s:text name="converter.billboard.update" /></span>
      </p>
    </div>
  </section>

  <section class="base-curr">
    <div class="container-lg">
      <div class="card">
        <h4 class="card-header"><s:text name="converter.baseCurr.header" /></h4>
        <div class="card-body py-4">
          <div class="row gx-4">
            <div class="col-12 col-sm-6">
              <h5><s:text name="converter.rates.currency" /></h5>
              <select class="form-select" id="base-curr">
                <c:forEach
                  items="${currencies}"
                  var="currency"
                  varStatus="loop"
                >
                  <c:choose>
                    <c:when test="${loop.index == 0}">
                      <option selected="selected" value="${currency}">
                        ${currency}
                      </option>
                    </c:when>
                    <c:otherwise>
                      <option value="${currency}">${currency}</option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </select>
            </div>
            <div class="col-12 col-sm-6">
              <h5><s:text name="converter.rates.amount" /></h5>
              <input
                type="text"
                placeholder="1.0000"
                class="form-control"
                id="base-amt"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="rates">
    <h4 class="text-center"><s:text name="converter.rates.header" /></h4>
    <div class="container-lg">
      <table class="table" aria-describedby="rates" id="rates">
        <thead>
          <tr>
            <th class="w-25" scope="col"><s:text name="converter.rates.currency" /></th>
            <th scope="col"><s:text name="converter.rates.rate" /></th>
            <th scope="col"><s:text name="converter.rates.amount" /></th>
          </tr>
        </thead>
        <tbody>
          <c:forEach items="${currencies}" var="currency" varStatus="loop">
            <tr class="${currency}">
              <th scope="row">${currency}</th>
              <td>-</td>
              <td>-</td>
            </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
  </section>

  <section class="disclaimer">
    <div class="container-lg">
      <h4 class="text-center"><s:text name="converter.disclaimer.header" /></h4>
      <s:text name="converter.disclaimer.content" />
    </div>
  </section>
</z:layout>
