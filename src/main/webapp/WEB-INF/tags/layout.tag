<%@ tag pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html
  xmlns="http://www.w3.org/1999/xhtml"
  lang="${request.locale.language}"
  xml:lang="${request.locale.language}"
>
  <head>
    <title><s:text name="converter.title" /></title>
    <base id="baseUrl" href="${baseUrl}/" />
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="assets/favicon.ico" type="image/x-icon" />

    <link
      href="//cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
      crossorigin="anonymous"
    />
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
      crossorigin="anonymous"
    ></script>
    <script
      src="//code.jquery.com/jquery-3.6.0.min.js"
      integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
      crossorigin="anonymous"
    ></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <link rel="preconnect" href="//fonts.gstatic.com" />
    <link
      href="//fonts.googleapis.com/css2?family=Inter:wght@100;400&display=swap"
      rel="stylesheet"
    />

    <link
      rel="stylesheet/less"
      type="text/css"
      href="assets/less/converter.less"
    />
    <script src="//cdn.jsdelivr.net/npm/less"></script>
    <script src="assets/js/converter.js"></script>
  </head>
  <body>
    <nav class="navbar fixed-top navbar-dark">
      <div class="container-lg">
        <div class="brand">
          <a href="#">
            <img src="assets/images/brand.png" alt="logo" />
            <span><s:text name="converter.title" /></span>
          </a>
        </div>
        <div class="local">
          <ul>
            <li>
              <a href="converter.action?request_locale=en">English</a>
            </li>
            <li>
              <a href="converter.action?request_locale=zh">繁</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <jsp:doBody />
    <footer class="footer">
      <div class="text-center p-3">
        © 2021 Copyright <a href="https://andytang.dev">andytang.dev</a>.
        Licensed under the <a href="license">MIT license</a>.
      </div>
    </footer>
  </body>
</html>
