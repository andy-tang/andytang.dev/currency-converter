var state = {
  baseUrl: '',
  currencies: [],

  rates: null,
  date: null,
  baseCurr: null,
  baseAmt: 1.0,
}

const stateHandler = {
  set: async (obj, prop, value) => {
    obj[prop] = value
    switch (prop) {
      case 'baseCurr':
        const { rates, date } = await fetchRates(stateProxy.baseCurr)
        stateProxy.rates = rates
        stateProxy.date = date
      // fall through
      case 'baseAmt':
      case 'rates':
        updateRates()
        return true
      case 'date':
        $('#date span').length === 1
          ? $('#date').append(`<span>${value.toLocaleDateString()}<span>`)
          : $('#date span:nth-child(2)').html(value.toLocaleDateString())
        return true
      default:
        return false
    }
  },
}

const stateProxy = new Proxy(state, stateHandler)

$(document).ready(() => {
  initUIComponents()
})

$(window).on('load', async () => {
  stateProxy.baseUrl = $('#baseUrl').attr('href')
  stateProxy.currencies = $.map($('#base-curr option'), function (option) {
    return option.value
  })

  // Set currency and amount default value
  if ($('#base-curr > option').length > 1) {
    stateProxy.baseCurr = $('#base-curr option:selected').val()
    stateProxy.baseAmt = 1.0
  }
})

const fetchRates = (base) => {
  return new Promise((resolve, reject) => {
    $.ajax(stateProxy.baseUrl + 'api/rates', {
      type: 'GET',
      data: {
        base,
      },
      complete: (jqXHR) => {
        try {
          const $xml = $($.parseXML(jqXHR.responseText))
          const date = new Date(
            parseInt($xml.find('ExRate').find('date').text())
          )
          const ratesXml = $xml.find('ExRate').find('rates')
          const rates = {}

          Array.from(ratesXml[0].childNodes).forEach((rate) => {
            rates[rate.tagName] = parseFloat(rate.textContent)
          })

          resolve({ rates, date })
        } catch (err) {
          reject(err)
        }
      },
    })
  })
}

const updateRates = () => {
  if (!!!stateProxy.rates) return

  Object.entries(stateProxy.rates)
    .filter(([key]) =>
      stateProxy.currencies.some((currency) => currency === key)
    )
    .forEach(([key, value]) => {
      const row = $(`#rates tbody tr.${key}`)?.children()
      $(row[1]).html(numeral(value).format('0,0.0000'))
      $(row[2]).html(
        numeral(value * stateProxy.baseAmt + Number.EPSILON).format('0,0.0000')
      )
      // $(row[2]).html(
      //   (
      //     Math.round((value * stateProxy.baseAmt + Number.EPSILON) * 10000) /
      //     10000
      //   ).toFixed(4)
      // )
    })
}

/* UI components */

const initUIComponents = () => {
  // Set event listeners
  $('#base-curr').change((e) => {
    stateProxy.baseCurr = $(e.target).find('option:selected').val()
  })

  $('#base-amt')
    .keypress((e) => {
      if (
        !(
          (e.which > 47 && e.which < 58) ||
          [8, 44].includes(e.which) ||
          (e.which === 46 &&
            ($(e.target).val().match(/\./g) || []).length === 0)
        )
      )
        e.preventDefault()
    })
    .keyup((e) => {
      if (!/^(?!0+\.0+$)\d{1,3}(?:,\d{3})*\.\d{4}$/.test($(e.target).val()))
        return
      stateProxy.baseAmt = numeral($(e.target).val()).value()
    })
    .change((e) => {
      const val = numeral($(e.target).val())
      stateProxy.baseAmt = val.value()
      $(e.target).val(val.format('0,0.0000'))
    })
}
