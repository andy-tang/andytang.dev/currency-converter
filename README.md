# Currency Converter with Apache Struts 2

| Service Name | crypto-prices                                                                                      |
| ------------ | -------------------------------------------------------------------------------------------------- |
| Language     | ![](https://img.shields.io/badge/Java-v8-EB2024?logo=java&style=flat)                              |
| Requisites   | N/A                                                                                                |
| Service Host | Demo: [https://demo.andytang.dev/currency-converter](https://demo.andytang.dev/currency-converter) |

## Keywords

- Java: `Apache Maven`, `Apache Struts 2 Framework`, `JSP`, `XPath`, `Jetty`
- Javascript: `jQuery`, `Proxy`
- Frontend: `Bootstrap 5`, `Less`, `AJAX`
- DevOps: `Docker`, `Kubernetes`, `GitLab CI/CD`

## Overview

Users can access the following information with the provided APIs and webpages:

- latest currency exchange rates

This service serves as an API proxy between the frontend and the actual backend services. Exchange rates data are fetched real time from European Central Bank before they are marshalled and served to the frontend.

This service is written in [Apache Struts 2 Framework](https://struts.apache.org/) with the offical [`struts2-archetype-convention`](https://struts.apache.org/maven-archetypes/#the-blank-convention-archetype-struts2-archetype-convention) template. To provide the original taste of Struts 2, `JSP` and `jQuery` are used to create a server side rendering and AJAX website.

## REST API List

- [REST APIs](doc/rest-apis.md)
  - [Get Latest Currency Rates](#get-latest-currency-rates)

## API Response Format

I am a sometime fundamentalist. Since we are creating an AJA**X** website, the API response is designed in XML format.

## Quick Start

### Start Jetty server

`mvn jetty:run`

### Start Server with specific port

`mvn jetty:run -Djetty.http.port=8090`
