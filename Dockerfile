# syntax = docker/dockerfile:experimental
#--------------------------------------------------------------------------------
# Stage 1: Build JAVA codes to war
#--------------------------------------------------------------------------------
FROM maven:3-openjdk-11 as builder
LABEL stage=intermediate

WORKDIR /opt/app
COPY . .
RUN --mount=type=cache,target=/root/.m2 mvn package -Dmaven.test.skip=true

#--------------------------------------------------------------------------------
# Stage 2: Prepare running environment for java container
#--------------------------------------------------------------------------------
FROM jetty:9-jre11-slim

COPY --from=builder /opt/app/target/currConverter.war /var/lib/jetty/webapps/root.war
EXPOSE 8090
CMD ["java", "-jar", "/usr/local/jetty/start.jar", "-Djetty.port=8090"]
